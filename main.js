
const treeSize = 100
const spreadX = 100
const spreadY = 50

const main = async d => {

  const canvas = d.getElementById("canvas")
  canvas.width = d.body.clientWidth
  canvas.height = d.body.clientHeight

  d.addEventListener("click", e => drawTree(canvas, treeSize, e.pageX, e.pageY))

  drawBackground(canvas)
  drawTree(canvas, treeSize, canvas.width/2, canvas.height)
}

const drawBackground = async canvas => {

  const c = canvas.getContext("2d")

  const [color1, color2] = [randomColor(), randomColor()]

  const gradient = c.createLinearGradient(0, 0, canvas.width, canvas.height)
  gradient.addColorStop(0, color1)
  gradient.addColorStop(1, color2)

  c.fillStyle = gradient
  c.fillRect(0, 0, canvas.width, canvas.height)
}

const randomColor = () =>
  '#' + Math.floor(Math.random()*(1<<24)).toString(16)

const drawTree = async (canvas, n, x, y) => {

  const c = canvas.getContext("2d")
  c.strokeStyle = randomColor()
  c.lineWidth = 1
  c.beginPath()

  await drawBranch(c, n, n, x, y, spreadX, spreadY)
}

const drawBranch = async (c, max, n, x, y, xs = 1, ys = 1) => {

  if (n <= 0) return

  c.moveTo(x, y)

  const dx = Math.round(Math.random() * xs) - (xs/2)
  const dy = Math.round(Math.random() * ys)
  x -= dx
  y -= dy
  
  c.quadraticCurveTo(x, y+dy, x, y)
  c.stroke()
  
  setTimeout(() => drawBranch(c, max, --n, x, y, xs, ys), 0)

  const branchAgain = !Math.round(Math.random() * (max-n))
  if (branchAgain) {
    setTimeout(() => drawBranch(c, max, --n, x, y, xs, ys), 0)
  }
}

main(document)
