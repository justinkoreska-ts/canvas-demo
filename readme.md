# Canvas Demo

Experiments with generating art from code.

## Fractals and Recursion

Inspiration from [Chapter 8 - Fractals](https://natureofcode.com/book/chapter-8-fractals/) of [The Nature of Code](https://natureofcode.com/).
